#Vivek 02/25/2015
CC=icc
info:
	@echo "info fooprog clean"	
fooprog : foo.o bar.o
	$(CC) -o fooprog foo.o bar.o -lm
foo.o : foo.c
	$(CC) -c foo.c
bar.o : bar.c
	$(CC) -c bar.c
clean :
	rm -f *.o fooprog
