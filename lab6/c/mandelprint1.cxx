void queue::main_loop(circle *workcircle) {
double start_time,end_time,elapsed;
start_time=omp_get_wtime();
  this->set_image
    ( new Image(workcircle->pixels,workcircle->pixels,"mandelpicture") );       
    #pragma omp parallel
    #pragma omp single
    {
        for (;;) 
	{
            struct coordinate xy; int res;
            workcircle->next_coordinate(xy);
            if (workcircle->is_valid_coordinate(xy)) 
            {
                this->total_tasks += 1; 
                #pragma omp task
                {
                    res = belongs(xy,workcircle->infty);
                    coordinate_to_image(xy,res);
                }                                    
            } 
            else break;
        }
     }    
     image->Write();
end_time=omp_get_wtime();
elapsed=end_time-start_time;
std::cout<<"Time taken = "<<elapsed<<std::endl;
}

