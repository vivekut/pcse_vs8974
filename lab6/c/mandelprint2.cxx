void queue::main_loop(circle *workcircle) {
double start_time,end_time,elapsed;
start_time=omp_get_wtime();
  this->set_image
    ( new Image(workcircle->pixels,workcircle->pixels,"mandelpicture") );
        
        #pragma omp parallel
        #pragma omp single
        {
	int count=0;
	struct coordinate xy_buf[128];
        for (;;) 
	{
            struct coordinate xy; int res;
            workcircle->next_coordinate(xy);
            if (workcircle->is_valid_coordinate(xy)) 
            {
                this->total_tasks += 1; 
		xy_buf[count]=xy;
		count++;
		if(count==128)
		{
                    #pragma omp task
                    {
			for(int j=0;j<count;j++)
			{
                            res = belongs(xy_buf[j],workcircle->infty);
                            coordinate_to_image(xy_buf[j],res);
			}
                    }
	            count=0;
                }
            } 
            else break;

        }
        }
    
  image->Write();

end_time=omp_get_wtime();
elapsed=end_time-start_time;
std::cout<<"Time taken = "<<elapsed<<std::endl;
}

