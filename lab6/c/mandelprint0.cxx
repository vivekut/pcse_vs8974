/** The main computational loop
 */
void queue::main_loop(circle *workcircle) {
double start_time,end_time,elapsed;
start_time=omp_get_wtime();
  this->set_image
    ( new Image(workcircle->pixels,workcircle->pixels,"mandelpicture") );
  for (;;) {
    struct coordinate xy; int res;
    workcircle->next_coordinate(xy);
    if (workcircle->is_valid_coordinate(xy)) {
      this->total_tasks += 1;
      res = belongs(xy,workcircle->infty);
      coordinate_to_image(xy,res);
    }
    else break;
  }

