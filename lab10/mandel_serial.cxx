#include <stdlib.h>
#include <stdio.h>
#include <mpi.h>

#include "mandel.h"
#include "Image.h"

/**
   A class for a queue where the master sends coordinates
   to the workers with blocking sends and receives.
   Of course this is very silly, but it only serves to
   provide a code base.
 */
class serialqueue : public queue {
private :
  int free_processor;
public :
  serialqueue(MPI_Comm queue_comm,circle *workcircle)
    : queue(queue_comm,workcircle) {
    free_processor=0;
  };
  /** 
      The `addtask' routine adds a task to the queue. In this
      simple case it immediately sends the task to a worker
      and waits for the result, which is added to the image.

      This routine is only called with valid coordinates;
      the calling environment will stop the process once
      an invalid coordinate is encountered.
  */
  int addtask(struct coordinate xy) {
    MPI_Status status; int contribution, err;

    err = MPI_Send(&xy,1,coordinate_type,
	     free_processor,0,comm); CHK(err);
    err = MPI_Recv(&contribution,1,MPI_INT,
	     free_processor,0,comm, &status); CHK(err);

    coordinate_to_image(xy,contribution);
    total_tasks++;
    free_processor = (free_processor+1)%(ntids-1);

    return 0;
  };
  int wait_and_write()
  {
      return 0;
  };
};

class bulkqueue : public queue {
private :
  int free_processor,task_buffer;
  int *con_buffer;
  struct coordinate* xy_buffer;
  MPI_Request *reqs_send, *reqs_recv;
  MPI_Status *stat_send, *stat_recv;
public :
  bulkqueue(MPI_Comm queue_comm,circle *workcircle)
    : queue(queue_comm,workcircle) {
    free_processor=0;
    task_buffer=0;
    xy_buffer=new struct coordinate[ntids-1];
    
    con_buffer=new int[ntids-1];
    reqs_send=new MPI_Request[ntids-1];
    reqs_recv=new MPI_Request[ntids-1];
    stat_send=new MPI_Status[ntids-1];
    stat_recv=new MPI_Status[ntids-1];

  };
 
  /** 
      The `addtask' routine adds a task to the queue. In this
      simple case it immediately sends the task to a worker
      and waits for the result, which is added to the image.

      This routine is only called with valid coordinates;
      the calling environment will stop the process once
      an invalid coordinate is encountered.
  */

  int addtask(struct coordinate xy) {
    MPI_Status status; int contribution, err;
    xy_buffer[task_buffer].x=xy.x;
    xy_buffer[task_buffer].y=xy.y;

    err = MPI_Isend(&xy_buffer[task_buffer],1,coordinate_type,
	     free_processor,0,comm,&reqs_send[task_buffer]); CHK(err);	    
    err = MPI_Irecv(&con_buffer[task_buffer],1,MPI_INT,
	     free_processor,0,comm, &reqs_recv[task_buffer]); CHK(err);

    //coordinate_to_image(xy,contribution);

    
    total_tasks++;
    task_buffer++;

    free_processor = (free_processor+1)%(ntids-1);
    if(task_buffer==(ntids-1))
    {
        MPI_Waitall(ntids-1,reqs_send,stat_send);
	MPI_Waitall(ntids-1,reqs_recv,stat_recv);
        for(int j=0;j<ntids-1;j++)
	{
	    coordinate_to_image(xy_buffer[j],con_buffer[j]);
	}
	free_processor=0;
	task_buffer=0;
    }
      
    return 0;
  };
 
  int wait_and_write()
  {
     MPI_Waitall(task_buffer,reqs_send,stat_send);
     MPI_Waitall(task_buffer,reqs_recv,stat_recv);
     for(int j=0;j<task_buffer;j++)
     {
         coordinate_to_image(xy_buffer[j],con_buffer[j]);
     }
     return 0;
     delete reqs_send;
     delete reqs_recv;
     delete stat_send;
     delete stat_recv;
     delete xy_buffer;
     delete con_buffer;
  };
};

class collqueue : public queue {
private :
  int free_processor,task_buffer;
  int *con_buffer,cont_dum;
  struct coordinate* xy_buffer,xy_dummy;
  MPI_Request *reqs_send, *reqs_recv;
  MPI_Status *stat_send, *stat_recv;
public :
  collqueue(MPI_Comm queue_comm,circle *workcircle)
    : queue(queue_comm,workcircle) {
    free_processor=0;
    task_buffer=0;
    xy_buffer=new struct coordinate[ntids];
    
    con_buffer=new int[ntids-1];
    reqs_send=new MPI_Request[ntids-1];
    reqs_recv=new MPI_Request[ntids-1];
    stat_send=new MPI_Status[ntids-1];
    stat_recv=new MPI_Status[ntids-1];

  };
 
  /** 
      The `addtask' routine adds a task to the queue. In this
      simple case it immediately sends the task to a worker
      and waits for the result, which is added to the image.

      This routine is only called with valid coordinates;
      the calling environment will stop the process once
      an invalid coordinate is encountered.
  */

  int addtask(struct coordinate xy) {
    MPI_Status status; int contribution, err;
    xy_buffer[task_buffer].x=xy.x;
    xy_buffer[task_buffer].y=xy.y;

   // err = MPI_Isend(&xy_buffer[task_buffer],1,coordinate_type,
//	     free_processor,0,comm,&reqs_send[task_buffer]); CHK(err);	    
   // err = MPI_Irecv(&con_buffer[task_buffer],1,MPI_INT,
//	     free_processor,0,comm, &reqs_recv[task_buffer]); CHK(err);

    //coordinate_to_image(xy,contribution);

    
    total_tasks++;
    task_buffer++;

    free_processor = (free_processor+1)%(ntids-1);
    if(task_buffer==(ntids-1))
    {        
        MPI_Scatter(&xy_buffer[0],1,coordinate_type,&xy_dummy,1,coordinate_type,ntids-1,comm);
	MPI_Gather(&cont_dum,1,MPI_INT,&con_buffer[0],1,MPI_INT,ntids-1,comm);
	for(int j=0;j<ntids-1;j++)
	{
	    coordinate_to_image(xy_buffer[j],con_buffer[j]);
	}
	free_processor=0;
	task_buffer=0;
    }
      
    return 0;
  };
 
  int wait_and_write()
  {
     MPI_Scatter(&xy_buffer[0],1,coordinate_type,&xy_dummy,1,coordinate_type,ntids-1,comm);
     MPI_Gather(&cont_dum,1,MPI_INT,&con_buffer[0],1,MPI_INT,ntids-1,comm);
     for(int j=0;j<task_buffer;j++)
     {
         coordinate_to_image(xy_buffer[j],con_buffer[j]);
     }
     return 0;
     delete reqs_send;
     delete reqs_recv;
     delete stat_send;
     delete stat_recv;
     delete xy_buffer;
     delete con_buffer;
  };
};

int main(int argc,char **argv) {
  MPI_Comm comm;
  int ntids,mytid, steps,iters,ierr;

  MPI_Init(&argc,&argv);
  comm = MPI_COMM_WORLD;
  MPI_Comm_set_errhandler(comm,MPI_ERRORS_RETURN);
  MPI_Comm_size(comm,&ntids);
  MPI_Comm_rank(comm,&mytid);

  ierr = parameters_from_commandline
    (argc,argv,comm,&steps,&iters);
  if (ierr) return MPI_Abort(comm,1);

  if (ntids==1) {
    printf("Sorry, you need at least two processors\n");
    return 1;
  }

  circle *workcircle = new circle(steps,iters,ntids-1);
  serialqueue *taskqueue = new serialqueue(comm,workcircle);
  //bulkqueue *taskqueue=new bulkqueue(comm,workcircle);
  //collqueue *taskqueue=new collqueue(comm,workcircle);

  taskqueue->main_loop(comm,workcircle);
  MPI_Finalize();
  return 0;
}
