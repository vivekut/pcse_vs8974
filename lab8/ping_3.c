#include "mpi.h"
#include<stdio.h>


int main(int argc, char *argv[])
{
    int ierr,numtasks,rank,len;
    int tag1,tag2,dest,src;
    MPI_Request reqs0[2],reqs1[2];
    char outchar,inchar;
    ierr=MPI_Init(&argc,&argv);
    MPI_Comm_size(MPI_COMM_WORLD,&numtasks);
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    MPI_Status Stat1[2],Stat2[2];
    
    if(rank==0)
    {
        outchar='a';
    }
    else if(rank==1)
    {
        outchar='b';
    }
 
    if(numtasks>2)
    {
        printf("Number of tasks exceeded specification Terminating \n");
	MPI_Abort(MPI_COMM_WORLD,ierr);
    }
    
    tag1=10;
    tag2=20;
    printf("Data on each process before message passing\n");
    printf("The out character on rank %d is %c\n",rank,outchar);
    printf("The in character on rank %d is %c\n",rank,inchar);
    if(rank==0)
    {
        dest=1;
	src=1;
	MPI_Isend(&outchar,1,MPI_CHAR,dest,tag1,MPI_COMM_WORLD,&reqs0[0]);
	MPI_Irecv(&inchar,1,MPI_CHAR,src,tag2,MPI_COMM_WORLD,&reqs0[1]);
        MPI_Waitall(2,reqs0,Stat1);
    }
    else if(rank==1)
    {
        src=0;
	dest=0;
	MPI_Isend(&outchar,1,MPI_CHAR,dest,tag2,MPI_COMM_WORLD,&reqs1[0]);
	MPI_Irecv(&inchar,1,MPI_CHAR,src,tag1,MPI_COMM_WORLD,&reqs1[1]);
        MPI_Waitall(2,reqs1,Stat2);
    }
    printf("\n");
    printf("Data on each process after message passing\n");
    printf("The out character on rank %d is %c\n",rank,outchar);
    printf("The in character on rank %d is %c\n",rank,inchar);

    MPI_Finalize();
}
