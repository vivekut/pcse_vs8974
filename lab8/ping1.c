#include "mpi.h"
#include<stdio.h>


int main(int argc, char *argv[])
{
    int ierr,numtasks,rank,len;
    int tag1,tag2,dest,src;
    char outchar,inchar;
    ierr=MPI_Init(&argc,&argv);
    MPI_Comm_size(MPI_COMM_WORLD,&numtasks);
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    MPI_Status Stat;
    
    if(rank==0)
    {
        outchar='a';
    }
    else if(rank==1)
    {
        outchar='b';
    }
 
    if(numtasks>2)
    {
        printf("Number of tasks exceeded specification Terminating \n");
	MPI_Abort(MPI_COMM_WORLD,ierr);
    }
    
    tag1=10;
    tag2=20;
    printf("Data on each process before message passing\n");
    printf("The out character on rank %d is %c\n",rank,outchar);
    printf("The in character on rank %d is %c\n",rank,inchar);
    if(rank==0)
    {
        dest=1;
	src=1;
	MPI_Send(&outchar,1,MPI_CHAR,dest,tag1,MPI_COMM_WORLD);
	MPI_Recv(&inchar,1,MPI_CHAR,src,tag2,MPI_COMM_WORLD,&Stat);
    }
    else if(rank==1)
    {
        src=0;
	dest=0;
	MPI_Send(&outchar,1,MPI_CHAR,dest,tag2,MPI_COMM_WORLD);
	MPI_Recv(&inchar,1,MPI_CHAR,src,tag1,MPI_COMM_WORLD,&Stat);
    }
    printf("\n");
    printf("Data on each process after message passing\n");
    printf("The out character on rank %d is %c\n",rank,outchar);
    printf("The in character on rank %d is %c\n",rank,inchar);

    printf("Printing the status information\n");
    printf("For the process %d the source id is %d and the message tag is %d\n",rank,Stat.MPI_SOURCE,Stat.MPI_TAG);

    MPI_Finalize();
}
