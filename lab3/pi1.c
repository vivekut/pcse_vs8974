/****************************************************************
 *
 * This program file is part of 
 * `Parallel Computing for Science and Engineering'
 * by Victor Eijkhout, copyright 2012/3/4/5
 *
 * pi1.c : initial file for the lab about loop computing
 *
 ****************************************************************/

#include <stdlib.h>
#include <stdio.h>
#include <math.h>
#include <omp.h>

int main(int argc,char **arg) {

  int nsteps=1000000000; // that's one billion
  double tstart,tend,elapsed, pi,quarterpi,h;
  
  
  tstart = omp_get_wtime(); //gettime();
  quarterpi = 0.; h = 1./nsteps;

#pragma omp parallel
{
   int thread_id,num_threads,i;
   num_threads=omp_get_num_threads();
   thread_id=omp_get_thread_num();
   int first_step,last_step;
   int step_spacing;
   step_spacing=nsteps/num_threads;
   first_step=thread_id*step_spacing;
   last_step=first_step+step_spacing;
   for (i=first_step; i<last_step; i++) {
    double
      x = i*h,
      y = sqrt(1-x*x);
    quarterpi += h*y;
  }
}
  pi = 4*quarterpi;
  tend = omp_get_wtime(); //gettime();
  elapsed = tend-tstart;
  printf("Computed pi=%e in %6.3f seconds\n",pi,elapsed);
  
  return 0;
}
