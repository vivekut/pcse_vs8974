#include "mpi.h"
#include<stdio.h>
#include<unistd.h>

int main(int argc,char *argv[])
{
    int numtasks,rank,len,ierr;
    char hostname[MPI_MAX_PROCESSOR_NAME];
    double t_start,t_end,t_elapsed;

    ierr=MPI_Init(&argc,&argv);
    if(ierr!=MPI_SUCCESS)
    {
        printf("Error starting MPI program Terminating\n")
	MPI_Abort(MPI_COMM_WORLD,ierr);
    }

    MPI_Comm_size(MPI_COMM_WORLD,&numtasks);
    MPI_Comm_rank(MPI_COMM_WORLD,&rank);
    MPI_Get_processor_name(hostname,&len);
    printf("Number of tasks= %d, My rank = %d Running on %s\n",numtasks,rank,hostname);
    t_start=MPI_Wtime();
    sleep(rank+1);
    t_end=MPI_Wtime();
    t_elapsed=t_end-t_start;
    printf("On rank %d time taken %f",rank,t_elapsed);

    MPI_Finalize();
    return 0;
}
