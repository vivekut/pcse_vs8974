#include<stdio.h>
#include "mpi.h"
#define NUM_POINTS 512
#define BOTTOM 0
#define RIGHT 1
#define TOP 2
#define LEFT 3
#define INDEX(i,j,n) (j) + (i)*(n)
#include<stdlib.h>
#include<math.h>
#include<stdbool.h>
#include<fstream>

class poisson_solver
{
    private:
    double *phi,*phi_c,*x,*y,local_res,global_res;
    double *e1_c,*e1_f,*e2_c,*e2_f,*e3_c,*e3_f;
    double *r1,*r2,*r3;
    int size,i,j,k;
    int numprocs,rank,row_procs,rank_x,rank_y,rank_x_next,rank_x_prev,rank_y_next,rank_y_prev;
    int num_row_points,num_col_points,top_ghost_row,right_ghost_col,row_size;
    int num_row_points_1,row_size_1;
    bool is_phys_boundary[4];
    double delta_x,delta_y,x_start,y_start;
    int tag1,tag2,tag3,tag4;
    MPI_Status Stat;
    MPI_Datatype PHI_COULMN;

    MPI_Comm comm;
    public:
    poisson_solver(MPI_Comm,int);
    void allocate_memory();
    void fill_boundary();
    void main_loop();
    void main_loop_mg();
    double *create_error(int,int);
    double *project(double *,int,int);
    void extrapolate(double *,double *,int,int );
    void smooth(double *,int,int,int,double *,MPI_Comm );
    double *calculate_residue(double *,int,int,int,double*);
    void v_cycle(int,int,double *,int,int,double *,MPI_Comm);
};

poisson_solver::poisson_solver(MPI_Comm communicator,int r_p)
{
    comm=communicator;
    tag1=1,tag2=2,tag3=3,tag4=4;
    MPI_Comm_rank(comm,&rank);
    MPI_Comm_size(comm,&numprocs);
    //Cartesian mapping
    row_procs=r_p;
    int col_procs;
    col_procs=numprocs/row_procs;
    rank_x=rank%row_procs;
    rank_y=rank/row_procs;
    rank_x_next=rank+1;
    rank_x_prev=rank-1;
    rank_y_next=rank+row_procs;
    rank_y_prev=rank-row_procs;
    for(i=0;i<3;i++)
    {
	is_phys_boundary[i]=false;
    }

    if(rank_y==0)
    {
        is_phys_boundary[BOTTOM]=true;
	rank_y_prev=MPI_PROC_NULL;
    }
    if(rank_x==0)
    {
        is_phys_boundary[LEFT]=true;
	rank_x_prev=MPI_PROC_NULL;
    }
    if(rank_x==(row_procs-1))
    {
        is_phys_boundary[RIGHT]=true;
	rank_x_next=MPI_PROC_NULL;
    }
    if(rank_y==(col_procs-1))
    {
        is_phys_boundary[TOP]=true;
	rank_y_next=MPI_PROC_NULL;
    }

    num_col_points=NUM_POINTS/row_procs;
    num_row_points=NUM_POINTS/col_procs;
    //printf("Number of colprocs %d and number of row_points %d\n",col_procs,num_row_points);
    //printf("Number of rowprocs %d and number of col_points %d\n",row_procs,num_col_points);
    row_size=num_col_points+2;
    MPI_Type_vector(num_row_points,1,row_size,MPI_DOUBLE,&PHI_COULMN);
    MPI_Type_commit(&PHI_COULMN);


} 
  /*  if(rank==0)
    printf("Row procs %d\n",row_procs);
    printf("For process (%d,%d) the next rank_y is %d and prev rank_y is %d\n",rank_x,rank_y,rank_y_next,rank_y_prev);
    printf("For process (%d,%d) the next rank_x is %d and prev rank_x is %d\n",rank_x,rank_y,rank_x_next,rank_x_prev);
  */   
    //Assigning points
void poisson_solver::allocate_memory()
{
    x=new double[num_col_points+2];
    y=new double[num_row_points+2];
    delta_x=1.0/(NUM_POINTS+1);
    delta_y=1.0/(NUM_POINTS+1);
    x_start=rank_x*num_col_points*delta_x;
    y_start=rank_y*num_row_points*delta_y;
    
   
    
    for(i=0;i<(num_col_points+2);i++)
    {
        x[i]= x_start + i*delta_x;
    }
   
    for(i=0;i<(num_row_points+2);i++)
    {
        y[i]=y_start + i*delta_y;
    }
    
    //Assigning values the phi array should have a provision to store the solution and the ghost array
    phi=new double[(num_row_points+2)*(num_col_points+2)];
    phi_c=new double[(num_row_points+2)*(num_col_points+2)];
   

    for(i=1;i<=num_row_points;i++)
    {
        for(j=1;j<=num_col_points;j++)
	{
	    phi[INDEX(i,j,row_size)]=0.0;
        }
    }

  for(i=0;i<=num_row_points+1;i++)
    {
        for(j=0;j<=num_col_points+1;j++)
	{
	    phi_c[INDEX(i,j,row_size)]=exp(x[j])*exp(-2.0*y[i]);
	}
    }

}      

void poisson_solver::fill_boundary()
{
    //filling up the boundary buffers

    if(is_phys_boundary[BOTTOM])
    {
       

        for(j=1;j<=num_col_points;j++)
	{
	    //phi[INDEX(0,j,row_size)]=x[j]*x[j]-y[0]*y[0];
            phi[INDEX(0,j,row_size)]=exp(x[j])*exp(-2.0*y[0]);
            //printf("Value written in %f\n",phi[INDEX(0,j,row_size)]);
	}
    }
    
    MPI_Send(&phi[INDEX(num_row_points,1,row_size)],num_col_points,MPI_DOUBLE,rank_y_next,tag1,comm);
    MPI_Recv(&phi[INDEX(0,1,row_size)],num_col_points,MPI_DOUBLE,rank_y_prev,tag1,comm,&Stat);
    
   // printf("Bottom boundary filled\n");
   // printf("Number of col points %d number of row points %d\n",num_col_points,num_row_points);
    top_ghost_row=num_row_points+1; 
    if(is_phys_boundary[TOP])
    {
        //printf("Filling up top boundary\n");
        for(j=1;j<=num_col_points;j++)
	{
	    //phi[INDEX(top_ghost_row,j,row_size)]=x[j]*x[j]-y[top_ghost_row]*y[top_ghost_row]; 
	    phi[INDEX(top_ghost_row,j,row_size)]=exp(x[j])*exp(-2.0*y[top_ghost_row]);
	    //printf("Value written in %f\n",phi[INDEX(0,j,row_size)]);

	}
    }
    
    MPI_Send(&phi[INDEX(1,1,row_size)],num_col_points,MPI_DOUBLE,rank_y_prev,tag2,comm);
    MPI_Recv(&phi[INDEX(top_ghost_row,1,row_size)],num_col_points,MPI_DOUBLE,rank_y_next,tag2,comm,&Stat);
    
    //printf("Top boundary filled\n");

    if(is_phys_boundary[LEFT])
    {
        for(i=1;i<=num_row_points;i++)
	{
	    //phi[INDEX(i,0,row_size)]=x[0]*x[0]-y[i]*y[i];
            phi[INDEX(i,0,row_size)]=exp(x[0])*exp(-2.0*y[i]);

	}
    }
    
    MPI_Send(&phi[INDEX(1,num_col_points,row_size)],1,PHI_COULMN,rank_x_next,tag3,comm);
    MPI_Recv(&phi[INDEX(1,0,row_size)],1,PHI_COULMN,rank_x_prev,tag3,comm,&Stat);
    
    //printf("Left boundary filled\n");
    right_ghost_col=num_col_points+1;
    if(is_phys_boundary[RIGHT])
    {
       	for(i=1;i<=num_row_points;i++)
	{
	      //phi[INDEX(i,right_ghost_col,row_size)]=x[right_ghost_col]*x[right_ghost_col] - y[i]*y[i];
              phi[INDEX(i,right_ghost_col,row_size)]=exp(x[right_ghost_col])*exp(-2.0*y[i]);
	}
    }
    
    MPI_Send(&phi[INDEX(1,1,row_size)],1,PHI_COULMN,rank_x_prev,tag4,comm);
    MPI_Recv(&phi[INDEX(1,right_ghost_col,row_size)],1,PHI_COULMN,rank_x_next,tag4,comm,&Stat);
    //printf("Receive complete\n");
    //printf("Boundaries filled up\n");
    
} 

void poisson_solver::main_loop()
{
    //This is just the version without multigrid to tes red-black method please ignore
    //printf("Right boundary filled\n")

    //Goal:: Write the red black non-blocking version of the code
    //printf("Entered main loop\n");
    for(k=0;k<1000;k++) 
    {
        local_res=0.0;
	global_res=0.0;
       
       //Iterating over the red points
       int start=1;
       for(i=1;i<=num_row_points;i++)
       {
           for(j=start;j<=num_col_points;j=j+2)
	   {
	       phi[INDEX(i,j,row_size)]=0.25*(phi[INDEX(i+1,j,row_size)] + phi[INDEX(i-1,j,row_size)]+phi[INDEX(i,j+1,row_size)]+phi[INDEX(i,j-1,row_size)])-5.0*delta_x*delta_y*exp(x[j])*exp(-2.0*y[i]);
	   }
	   start=1+(start%2);
       }
       //printf("Completed iterations over reds\n"); 
       //Communicating updated values
   
       MPI_Send(&phi[INDEX(num_row_points,1,row_size)],num_col_points,MPI_DOUBLE,rank_y_next,tag1,comm);
       MPI_Recv(&phi[INDEX(0,1,row_size)],num_col_points,MPI_DOUBLE,rank_y_prev,tag1,comm,&Stat);
      
       MPI_Send(&phi[INDEX(1,1,row_size)],num_col_points,MPI_DOUBLE,rank_y_prev,tag2,comm);
       MPI_Recv(&phi[INDEX(top_ghost_row,1,row_size)],num_col_points,MPI_DOUBLE,rank_y_next,tag2,comm,&Stat);
    
       MPI_Send(&phi[INDEX(1,num_col_points,row_size)],1,PHI_COULMN,rank_x_next,tag3,comm);
       MPI_Recv(&phi[INDEX(1,0,row_size)],1,PHI_COULMN,rank_x_prev,tag3,comm,&Stat);
    
       MPI_Send(&phi[INDEX(1,1,row_size)],1,PHI_COULMN,rank_x_prev,tag4,comm);
       MPI_Recv(&phi[INDEX(1,right_ghost_col,row_size)],1,PHI_COULMN,rank_x_next,tag4,comm,&Stat);
      
       //Iterating over the black points
       start=2;
       for(i=1;i<=num_row_points;i++)
       {
           for(j=start;j<=num_col_points;j=j+2)
	   {
	       phi[INDEX(i,j,row_size)]=0.25*(phi[INDEX(i+1,j,row_size)] + phi[INDEX(i-1,j,row_size)]+phi[INDEX(i,j+1,row_size)]+phi[INDEX(i,j-1,row_size)])-5.0*delta_x*delta_y*exp(x[j])*exp(-2.0*y[i]);
	   }
	   start=1+(start%2);
       }

       //Communicating updated values
       
       MPI_Send(&phi[INDEX(num_row_points,1,row_size)],num_col_points,MPI_DOUBLE,rank_y_next,tag1,comm);
       MPI_Recv(&phi[INDEX(0,1,row_size)],num_col_points,MPI_DOUBLE,rank_y_prev,tag1,comm,&Stat);
      
       MPI_Send(&phi[INDEX(1,1,row_size)],num_col_points,MPI_DOUBLE,rank_y_prev,tag2,comm);
       MPI_Recv(&phi[INDEX(top_ghost_row,1,row_size)],num_col_points,MPI_DOUBLE,rank_y_next,tag2,comm,&Stat);
    
       MPI_Send(&phi[INDEX(1,num_col_points,row_size)],1,PHI_COULMN,rank_x_next,tag3,comm);
       MPI_Recv(&phi[INDEX(1,0,row_size)],1,PHI_COULMN,rank_x_prev,tag3,comm,&Stat);
    
       MPI_Send(&phi[INDEX(1,1,row_size)],1,PHI_COULMN,rank_x_prev,tag4,comm);
       MPI_Recv(&phi[INDEX(1,right_ghost_col,row_size)],1,PHI_COULMN,rank_x_next,tag4,comm,&Stat);
      

       //calculating the residue
       for(i=1;i<=num_row_points;i++)
       {
           for(j=1;j<=num_col_points;j++)
	   {
	       local_res+=(phi[INDEX(i,j,row_size)]-phi_c[INDEX(i,j,row_size)])*(phi[INDEX(i,j,row_size)]-phi_c[INDEX(i,j,row_size)]);
	   }
       }
       local_res=local_res;
       MPI_Allreduce(&local_res,&global_res,1,MPI_DOUBLE,MPI_SUM,comm);
       //if(rank==0)
       //{
       //if(k==9)
         //  printf("local residue on rank %d is %f, where startr_x %f start_y %f, delta_x %f\n",rank,local_res,x_start,y_start,delta_x);
       //}
     
    }
    global_res=global_res/(NUM_POINTS*NUM_POINTS);
    if(rank==0)
    printf("Global residue %f\n",global_res);
    
    //printf("Exited main iteration loop\n");
    free(phi);
    free(phi_c);
    free(x);
    free(y);
    //printf("About to call finalize on rank %d\n",rank);
}

void poisson_solver::main_loop_mg()
{
    local_res=0.0;
    global_res=0.0;

    for(i=1;i<=num_row_points;i++)
    {
	for(j=1;j<=num_col_points;j++)
	{
	    local_res+=(phi[INDEX(i,j,row_size)]-phi_c[INDEX(i,j,row_size)])*(phi[INDEX(i,j,row_size)]-phi_c[INDEX(i,j,row_size)]);
	}
    }
   
    MPI_Allreduce(&local_res,&global_res,1,MPI_DOUBLE,MPI_SUM,comm);
    global_res=global_res/(NUM_POINTS*NUM_POINTS);
    if(rank==0)
    printf("Global residue in solution before mg %f\n",global_res);

    double *f=new double[(num_row_points+2)*(num_col_points+2)];
    for(i=1;i<=num_row_points;i++)
    {
        for(j=1;j<=num_col_points;j++)
	{
	    f[INDEX(i,j,row_size)]=5.0*exp(x[j])*(exp(-2.0*y[i]));
	}
    }
    //printf("Assigned source term\n"); 
    global_res=1.0;
    while(global_res>0.00001)
    {
          
	v_cycle(3,0,phi,num_row_points,num_col_points,f,comm);
	  
	global_res=0;
	local_res=0;
	for(i=1;i<=num_row_points;i++)
	{
	    for(j=1;j<=num_col_points;j++)
	    {
		local_res+=(phi[INDEX(i,j,row_size)]-phi_c[INDEX(i,j,row_size)])*(phi[INDEX(i,j,row_size)]-phi_c[INDEX(i,j,row_size)]);
	    }
	}
	local_res=local_res/(NUM_POINTS*NUM_POINTS);
	MPI_Allreduce(&local_res,&global_res,1,MPI_DOUBLE,MPI_SUM,comm);

	global_res=global_res;
	if(rank==0)
	printf("Global residue calculated by solution after mg %f\n",global_res);
    }
    //printf("Exited main iteration loop\n");
    free(phi);
    free(phi_c);
    free(x);
    free(y);
    //printf("About to call finalize on rank %d\n",rank);
}

double *poisson_solver::create_error(int num_row_points_l,int num_col_points_l)
{
    double *e_l=new double[(num_row_points_l+2)*(num_col_points_l+2)];
    int row_size_l=num_col_points_l+2;
    for(i=0;i<num_row_points_l+1;i++)
    {
        for(j=0;j<num_col_points_l+1;j++)
	{
            e_l[INDEX(i,j,row_size_l)]=0.0;
	}
    }
    return e_l;
}

void poisson_solver::v_cycle(int num_levels,int level,double *error_fine,int num_row_points_f,int num_col_points_f,double *source_fine,MPI_Comm comm)
{
    double *res_fine,*res_coarse,*error_coarse;
    int num_row_points_c,level_next,num_col_points_c;
        if(level==num_levels)
        {
	    return;
	}
        else
        { 
            num_row_points_c=num_row_points_f/2;
	    num_col_points_c=num_col_points_f/2;
	    level_next=level+1;           
	    smooth(source_fine,num_row_points_f,num_col_points_f,level,error_fine,comm);
            res_fine=calculate_residue(error_fine,num_row_points_f,num_col_points_f,level,source_fine);
	    res_coarse=project(res_fine,num_row_points_f,num_col_points_f);
	    error_coarse=create_error(num_row_points_c,num_col_points_c);
	    smooth(res_coarse,num_row_points_c,num_col_points_c,level_next,error_coarse,comm);
	    v_cycle(num_levels,level_next,error_coarse,num_row_points_c,num_col_points_c,res_coarse,comm);
	    extrapolate(error_fine,error_coarse,num_row_points_f,num_col_points_f);
            smooth(source_fine,num_row_points_f,num_col_points_f,level,error_fine,comm);
            free(error_coarse);
	    free(res_coarse);
            free(res_fine);
            return;
        }
}

double *poisson_solver::calculate_residue(double *vec,int num_row_points_l,int num_col_points_l,int level,double *res_l)
{
   
    int row_size=num_col_points_l+2;
    int col_size=num_row_points_l+2;
    double *res_temp_l=new double[row_size*col_size];
    if(level==0)
    {
	for(i=1;i<=num_row_points_l;i++)
	{
	    for(j=1;j<=num_col_points_l;j++)
	    {
		res_temp_l[INDEX(i,j,row_size)]=(-(vec[INDEX(i+1,j,row_size)]+vec[INDEX(i-1,j,row_size)]+vec[INDEX(i,j+1,row_size)]+vec[INDEX(i,j-1,row_size)])+4*vec[INDEX(i,j,row_size)])/(delta_x*delta_x)+ res_l[INDEX(i,j,row_size)];
	    }
	}
    }
    else
    {
	for(i=1;i<=num_row_points_l;i++)
        {
		for(j=1;j<=num_col_points_l;j++)
		{
	            res_temp_l[INDEX(i,j,row_size)]=(-(vec[INDEX(i+1,j,row_size)]+vec[INDEX(i-1,j,row_size)]+vec[INDEX(i,j+1,row_size)]+vec[INDEX(i,j-1,row_size)])+4*vec[INDEX(i,j,row_size)])/(pow(2,level)*pow(2,level)*delta_x*delta_x) + res_l[INDEX(i,j,row_size)];
		}
	}
    }
    return res_temp_l;
}


double * poisson_solver::project(double *fine,int row_size,int col_size)
{
    int res_row=col_size/2 + 2;
    int num_row_res=row_size/2,num_col_res=col_size/2;
    int fine_row=col_size+2;
    int i_f,j_f;
    double *res=new double[(num_row_res+2)*(num_col_res+2)];
    double res_norm=0.0;
    
    for(i=1;i<=num_row_res;i++)
    {
        for(j=1;j<=num_col_res;j++)
	{
	    i_f=2*i;
	    j_f=2*j;
	    res[INDEX(i,j,res_row)]=0.125*(4*fine[INDEX(i_f,j_f,fine_row)]+fine[INDEX(i_f-1,j_f,fine_row)]+fine[INDEX(i_f+1,j_f,fine_row)]+fine[INDEX(i_f,j_f-1,fine_row)] + fine[INDEX(i_f,j_f+1,fine_row)]);
        }
    }
    return res;
}

void  poisson_solver::smooth(double *res,int num_row_points_l,int num_col_points_l,int level,double *e_l,MPI_Comm comm)
{
    double local_res_l,global_res_l;
    int start;
    int row_size_l=num_col_points_l+2;
    
   
    MPI_Datatype E_COULMN;
    MPI_Type_vector(num_row_points_l,1,row_size_l,MPI_DOUBLE,&E_COULMN);
    MPI_Type_commit(&E_COULMN);
    for(k=0;k<10;k++) 
    {
        local_res=0.0;
	global_res=0.0;
	int top_ghost_row_l=num_row_points_l+1;
	int right_ghost_col_l=num_col_points_l+1;
       
       //Iterating over the red points
       //printf("Iterated over the coarse reds\n");
       start=1;
       for(i=1;i<=num_row_points_l;i++)
       {
           for(j=start;j<=num_col_points_l;j=j+2)
	   {
	       e_l[INDEX(i,j,row_size_l)]=0.25*(e_l[INDEX(i+1,j,row_size_l)] + e_l[INDEX(i-1,j,row_size_l)]+e_l[INDEX(i,j+1,row_size_l)]+e_l[INDEX(i,j-1,row_size_l)] - pow(2,level)*pow(2,level)*delta_x*delta_x*res[INDEX(i,j,row_size_l)]);
	   }
	   start=1+(start%2);
       }
       //printf("Iterated over reds\n"); 
       MPI_Send(&e_l[INDEX(num_row_points_l,1,row_size_l)],num_col_points_l,MPI_DOUBLE,rank_y_next,tag1,comm);
       MPI_Recv(&e_l[INDEX(0,1,row_size_l)],num_col_points_l,MPI_DOUBLE,rank_y_prev,tag1,comm,&Stat);
      
       MPI_Send(&e_l[INDEX(1,1,row_size_l)],num_col_points_l,MPI_DOUBLE,rank_y_prev,tag2,comm);
       MPI_Recv(&e_l[INDEX(top_ghost_row_l,1,row_size_l)],num_col_points_l,MPI_DOUBLE,rank_y_next,tag2,comm,&Stat);
    
       MPI_Send(&e_l[INDEX(1,num_col_points_l,row_size_l)],1,E_COULMN,rank_x_next,tag3,comm);
       MPI_Recv(&e_l[INDEX(1,0,row_size_l)],1,E_COULMN,rank_x_prev,tag3,comm,&Stat);
    
       MPI_Send(&e_l[INDEX(1,1,row_size_l)],1,E_COULMN,rank_x_prev,tag4,comm);
       MPI_Recv(&e_l[INDEX(1,right_ghost_col_l,row_size_l)],1,E_COULMN,rank_x_next,tag4,comm,&Stat);
       
       //printf("Iterated over the coarse blacks\n");
       start=2;
       for(i=1;i<=num_row_points_l;i++)
       {
           for(j=start;j<=num_col_points_l;j=j+2)
	   {
	       e_l[INDEX(i,j,row_size_l)]=0.25*(e_l[INDEX(i+1,j,row_size_l)] + e_l[INDEX(i-1,j,row_size_l)]+e_l[INDEX(i,j+1,row_size_l)]+e_l[INDEX(i,j-1,row_size_l)] - pow(2,level)*pow(2,level)*delta_x*delta_x*res[INDEX(i,j,row_size_l)]);
	   }
	   start=1+(start%2);
       }
       //printf("Iterated over blacks\n"); 
       MPI_Send(&e_l[INDEX(num_row_points_l,1,row_size_l)],num_col_points_l,MPI_DOUBLE,rank_y_next,tag1,comm);
       MPI_Recv(&e_l[INDEX(0,1,row_size_l)],num_col_points_l,MPI_DOUBLE,rank_y_prev,tag1,comm,&Stat);
      
       MPI_Send(&e_l[INDEX(1,1,row_size_l)],num_col_points_l,MPI_DOUBLE,rank_y_prev,tag2,comm);
       MPI_Recv(&e_l[INDEX(top_ghost_row_l,1,row_size_l)],num_col_points_l,MPI_DOUBLE,rank_y_next,tag2,comm,&Stat);
    
       MPI_Send(&e_l[INDEX(1,num_col_points_l,row_size_l)],1,E_COULMN,rank_x_next,tag3,comm);
       MPI_Recv(&e_l[INDEX(1,0,row_size_l)],1,E_COULMN,rank_x_prev,tag3,comm,&Stat);
    
       MPI_Send(&e_l[INDEX(1,1,row_size_l)],1,E_COULMN,rank_x_prev,tag4,comm);
       MPI_Recv(&e_l[INDEX(1,right_ghost_col_l,row_size_l)],1,E_COULMN,rank_x_next,tag4,comm,&Stat);
}     

   MPI_Type_free(&E_COULMN);
}

void poisson_solver::extrapolate(double *fine,double *coarse,int fine_row_points,int fine_col_points)
{
   //printf("Entered extrapolate\n");
   int i_c,j_c,j_p,j_m,i_p,i_m;
   int row_size_f=fine_col_points+2;
   int row_size_c=fine_col_points/2 +2;
   for(i=1;i<=fine_row_points;i++)
   {
       for(j=1;j<=fine_col_points;j++)
       {
           if(i%2==0 && j%2==0)
	   {
	      i_c=i/2;
	      j_c=j/2;
              fine[INDEX(i,j,row_size_f)]+=(coarse[INDEX(i_c,j_c,row_size_c)]);/* + coarse[INDEX(i_c+1,j_c,row_size_c)] + coarse[INDEX(i_c-1,j_c,row_size_c)] + coarse[INDEX(i_c,j_c+1,row_size_c)] + coarse[INDEX(i_c,j_c-1,row_size_c)]);*/
	   }
	   else if(i%2==0)
	   {
	       i_c=i/2;
	       j_p=(j+1)/2;
	       j_m=(j-1)/2;
	       fine[INDEX(i,j,row_size_f)]+=0.5*(coarse[INDEX(i_c,j_p,row_size_c)] + coarse[INDEX(i_c,j_m,row_size_c)]);// + coarse[INDEX(i_c+1,j_p,row_size_c)] +coarse[INDEX(i_c+1,j_m,row_size_c)] + coarse[INDEX(i_c-1,j_p,row_size_c)] + coarse[INDEX(i_c-1,j_m,row_size_c)]);
	   }
	   else if(j%2==0)
	   {
	       j_c=j/2;
	       i_p=(i+1)/2;
	       i_m=(i-1)/2;
               fine[INDEX(i,j,row_size_f)]+=0.5*(coarse[INDEX(i_p,j_c,row_size_c)] + coarse[INDEX(i_m,j_c,row_size_c)]);// + coarse[INDEX(i_p,j_c+1,row_size_c)] + coarse[INDEX(i_m,j_c+1,row_size_c)] + coarse[INDEX(i_p,j_c-1,row_size_c)] +coarse[INDEX(i_m,j_c-1,row_size_c)]);
	   }
	   else
	   {
	       i_p=(i+1)/2;
	       i_m=(i-1)/2;
	       j_p=(j+1)/2;
	       j_m=(j-1)/2;
	       fine[INDEX(i,j,row_size_f)]+=0.25*(coarse[INDEX(i_p,j_p,row_size_c)] + coarse[INDEX(i_p,j_m,row_size_c)] +coarse[INDEX(i_m,j_p,row_size_c)] + coarse[INDEX(i_m,j_m,row_size_c)]);
	   }

       }
   }
}

int main(int argc, char*argv[])
{
   MPI_Comm comm;
   MPI_Init(&argc,&argv);
   comm=MPI_COMM_WORLD;
   int row_procs;
   double start_time,end_time,total_time;
   start_time=MPI_Wtime();
   row_procs=atoi(argv[1]);
   poisson_solver *solve=new poisson_solver(comm,row_procs);
   solve->allocate_memory();
   solve->fill_boundary();
   solve->main_loop_mg();
   end_time=MPI_Wtime();
   total_time=end_time-start_time;
   int rank;
   MPI_Comm_rank(comm,&rank);
   if(rank==0)
   {
       printf("Total time taken %f s\n",total_time);
       std::ofstream result;
       result.open("res.dat");
       result<<total_time<<std::endl;
   }
   MPI_Finalize();

   return 0;
}
