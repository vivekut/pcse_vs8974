//Source code for the fifth problem
#include<omp.h>
#include<stdio.h>


int main(int arc, char *argv[]){

int thread_id,num_threads,num_procs;

FILE *fp;
fp=fopen("problem_7.txt","w"); //Creating the file which where the output is written

num_procs=omp_get_num_procs();
fprintf(fp,"The maximum number of threads that the hardware can support is %d\n",num_procs);
return 0;

}
