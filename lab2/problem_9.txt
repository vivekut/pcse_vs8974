outside parallel region of main program in thread 0 of 1. Parallel subrogram is called next
Inside subprogram in thread 0 out of 4
Inside subprogram in thread 1 out of 4
Inside subprogram in thread 2 out of 4
Inside subprogram in thread 3 out of 4
Inside a parallel region of main program in thread 3 out of 4. Parallel subprogram is called next
Inside a parallel region of main program in thread 1 out of 4. Parallel subprogram is called next
Inside a parallel region of main program in thread 2 out of 4. Parallel subprogram is called next
Inside a parallel region of main program in thread 0 out of 4. Parallel subprogram is called next
Inside subprogram in thread 2 out of 4
Inside subprogram in thread 1 out of 4
Inside subprogram in thread 0 out of 4
Inside subprogram in thread 3 out of 4
Inside subprogram in thread 2 out of 4
Inside subprogram in thread 1 out of 4
Inside subprogram in thread 0 out of 4
Inside subprogram in thread 3 out of 4
Inside subprogram in thread 0 out of 4
Inside subprogram in thread 2 out of 4
Inside subprogram in thread 1 out of 4
Inside subprogram in thread 3 out of 4
Inside subprogram in thread 0 out of 4
Inside subprogram in thread 1 out of 4
Inside subprogram in thread 3 out of 4
Inside subprogram in thread 2 out of 4


Note: Setting the omp_nested environment variable to true yeilds the 
expected number of outputs from the code. There are 25 outputs seen in 
this execution of the code.
