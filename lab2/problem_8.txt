outside parallel region of main program in thread 0 of 1. Parallel subrogram is called next
Inside subprogram in thread 0 out of 4
Inside subprogram in thread 1 out of 4
Inside subprogram in thread 2 out of 4
Inside subprogram in thread 3 out of 4
Inside a parallel region of main program in thread 3 out of 4. Parallel subprogram is called next
Inside a parallel region of main program in thread 1 out of 4. Parallel subprogram is called next
Inside a parallel region of main program in thread 2 out of 4. Parallel subprogram is called next
Inside a parallel region of main program in thread 0 out of 4. Parallel subprogram is called next
Inside subprogram in thread 0 out of 1
Inside subprogram in thread 0 out of 1
Inside subprogram in thread 0 out of 1
Inside subprogram in thread 0 out of 1


Note: The number of expected outputs from the subprogram is 4 
for when it is called from the main program and 4 for each time 
it is called from the parallel part of the main program. This makes
the total number of outputs from the subprogram as 20. This in addition
to the 5 outputs from the main program (1 serial + 4 parallel) makes
the total number of outputs as 25. This is not observed in the program.
The parallel part of the subprogram is executed on a single thread when 
called from the parallel part of the main program.
