//Source code for problem 8 and 9
#include<omp.h>
#include<stdio.h>

void parallel_sub(FILE *f)
{
int thread_id_l,num_threads_l;

#pragma omp parallel  
{
    num_threads_l=omp_get_num_threads();
    thread_id_l=omp_get_thread_num(); 
    fprintf(f,"Inside subprogram in thread %d out of %d\n",thread_id_l,num_threads_l);
}

}


int main(int arc, char *argv[]){

int thread_id,num_threads,num_procs;
num_procs=omp_get_num_procs();
FILE *fp;
fp=fopen("problem_8.txt","w");
thread_id=omp_get_thread_num();
num_threads=omp_get_num_threads();
fprintf(fp,"outside parallel region of main program in thread %d of %d. Parallel subrogram is called next\n",thread_id,num_threads);
parallel_sub(fp);
#pragma omp parallel private(thread_id,num_threads)
{
   num_threads=omp_get_num_threads();
   thread_id=omp_get_thread_num();
 
   fprintf(fp,"Inside a parallel region of main program in thread %d out of %d. Parallel subprogram is called next\n",thread_id,num_threads);
   parallel_sub(fp);
  
}



return 0;

}
