//Source code for the third problem 
#include<omp.h>
#include<stdio.h>


int main(int arc, char *argv[]){


FILE *fp;
fp=fopen("problem_3.txt","w"); //Creating the file which where the output is written

#pragma omp parallel 
{
   int num_threads,thread_id;
   num_threads=omp_get_num_threads();
   thread_id=omp_get_thread_num();
   
   if(thread_id==0)
   {  
       fprintf(fp,"Hello world from thread %d out of  %d\n",thread_id,num_threads);
   }
}

return 0;

}
