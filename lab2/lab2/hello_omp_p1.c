#include<omp.h>
#include<stdio.h>


int main(int arc, char *argv[]){

int thread_id,num_threads,num_procs;

FILE *fp;
fp=fopen("problem_1.txt","w"); //Creating the file which where the output is written


#pragma omp parallel private(thread_id,num_threads)
{
   num_threads=omp_get_num_threads();
   thread_id=omp_get_thread_num();
     
   fprintf(fp,"Hello from thread %d out of total number of threads %d\n",thread_id,num_threads);
   
}

return 0;

}
