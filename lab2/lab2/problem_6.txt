The maximum number of threads that the hardware can support is 16

Note: This was the output obtained from the same code as the previous question
but with setting the number of threads to 18. It is not possible to increase
the number of processors beyond the value obtained in the previous section
without using co processors.
