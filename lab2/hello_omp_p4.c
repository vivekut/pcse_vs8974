//Source code for the fourth problem 
#include<omp.h>
#include<stdio.h>


int main(int arc, char *argv[]){

int if_parallel,num_threads,thread_id;
FILE *fp;
fp=fopen("problem_4.txt","w"); //Creating the file which where the output is written

#pragma omp parallel 
{
   num_threads=omp_get_num_threads();
   thread_id=omp_get_thread_num();
   if_parallel=omp_in_parallel();
 
   if(if_parallel!=0)
   {  
       fprintf(fp,"Inside parallel region hello from thread %d out of  %d\n",thread_id,num_threads);
   }
}

num_threads=omp_get_num_threads();
thread_id=omp_get_thread_num();
if_parallel=omp_in_parallel();
if(if_parallel==0)
{
    fprintf(fp,"\nOutside parallel region hello from thread %d out of %d\n",thread_id,num_threads);
}



return 0;

}
