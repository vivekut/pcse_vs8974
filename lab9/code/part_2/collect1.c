#include<stdio.h>
#include "mpi.h"
#define NPROCS  4
#define ASIZE   8
#define ASIZEPP (ASIZE / NPROCS)
#define ROOT    2

int main(int argc, char *argv[])
{
    double master_vec[ASIZE], local_vec[ASIZEPP];
    int i, j, rank, numprocs,root;
    double root_sum=0, local_sum=0, global_sum=0;
    
    MPI_Init(&argc,&argv);
    MPI_Comm_size(MPI_COMM_WORLD, &numprocs);
    MPI_Comm_rank(MPI_COMM_WORLD, &rank);
    root=ROOT;
    
    if(rank==ROOT)
    {
        for(i=0;i<ASIZE;i++)
	{
	    master_vec[i]=100;
	}
    }
    else
    {
        for(i=0;i<ASIZE;i++)
	{
	    master_vec[i]=-1;
	}
    }
    
    MPI_Scatter(&master_vec[0],2,MPI_DOUBLE,&local_vec[0],2,MPI_DOUBLE,ROOT,MPI_COMM_WORLD);
    for(j=0;j<ASIZEPP;j++)
    {
        local_vec[j]+=rank;
    }
    
    printf("Printing local information\n");
    for(i=0; i<numprocs; i++)
    {
        if(rank == i)
	{
            printf("***(Rank %i): local_vec Output:\n",rank);
            for(j = 0; j < ASIZEPP; j++)
	    {
                printf("(Rank %i): %i %f\n",rank,j,local_vec[j]);
            }
         }
         MPI_Barrier(MPI_COMM_WORLD);
    }

    printf("Done printing local information\n");
    printf("\n");

    for(j=0;j<ASIZEPP;j++)
    {
        local_sum+=local_vec[j];
    }
    
    MPI_Reduce(&local_sum,&global_sum,1,MPI_DOUBLE,MPI_SUM,ROOT,MPI_COMM_WORLD);
    MPI_Gather(&local_vec[0],2,MPI_DOUBLE,&master_vec[0],2,MPI_DOUBLE,ROOT,MPI_COMM_WORLD);
    if(rank==ROOT)
    {
        for(i=0;i<ASIZE;i++)
	{
	    root_sum+=master_vec[i];
	}
    }
    printf("\n");
    printf("Printing the global information\n");
    
    for(i=0; i<numprocs; i++)
    {
        if(rank == i)
	{
            printf("***(Rank %i): master_vec Output:\n",rank);
            for(j = 0; j < ASIZE; j++)
	    {
                printf("(Rank %i): %i %f\n",rank,j,master_vec[j]);
            }
            printf("(Rank %i): The root process is: %i\n",rank,ROOT);
            printf("(Rank %i): Root            Sum:%f \n",rank,root_sum);
            printf("(Rank %i): MPI_Reduce      Sum:%f \n",rank,global_sum);


         }
         MPI_Barrier(MPI_COMM_WORLD);
    }
   


    MPI_Finalize();

}





